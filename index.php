<?php  session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <title>Дом Престарелых "Солидный Возраст"</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-theme.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/slick.css" rel="stylesheet">
    <link href="/css/slick-theme.css" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800&amp;subset=cyrillic" rel="stylesheet">
</head>
<body>

<?php if (isset($_SESSION['message_result'])){ ?>
<div id="message_alert" class="alert alert-<?php print $_SESSION['message_result']['class']; ?> alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="h2 text-center"> <?php print $_SESSION['message_result']['text']; ?></p>
</div>
<?php  unset($_SESSION['message_result']); }  ?>

<header id="header">
    <div class="container">
        <div class="row">
            <div class="col-md-1">  <img src="/img/logo.png" class="img-responsive logo"></div>
            <div class="col-md-4">
                <p class="h4  text-uppercase strong">Солидный возраст</p>
                <p class="h5 mt-none">Центр ухода за пожилыми людьми в г. Клин</p>
            </div>
<div class="col-md-3"></div>
            <div class="col-md-4 text-center">
                <p class="h2 strong color-black mt-none" data-value="app_phone">8 (925) 391-00-57</p>
                <p class="h5" data-value="app_address">Московская область, Клинский р-н, д. Решоткино, ул. Весенняя, стр. 57</p>
            </div>
        </div>
    </div>

    <nav class="navbar navbar-default">
        <div class="container">
            <div class="row">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="#">Главная</a></li>
                        <li><a href="#">Условия размещения</a></li>
                        <li><a href="#">Персонал и врачи</a></li>
                        <li><a href="#">Фото и видео</a></li>
                        <li><a href="#">Отзывы</a></li>
                        <li><a href="#">Контакты</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </nav>
</header>

<section id="promo">
    <div class="container">
        <div class="row mt-xlg">
            <div class="col-md-4">
                <div class="form">
                    <p class="h4 text-center">Оставьте заявку на консультацию и визит в наш дом!</p>
                    <div class="form-group">
                        <input type="tel" class="form-control" placeholder="Ваш телефон">
                    </div>
                    <button class="sender form-control btn btn-warning">Оставить заявку</button>
                </div>
            </div>
            <div class="col-md-6 mt-none">
                <p class="h1 text-uppercase color-white strong wow zoomIn">Я хочу достойную старость</p>
                <p class="h3 mb-xlg color-white wow fadeIn" data-wow-delay="0.9s">Для моей МАМЫ</p>
                <div class="row">
                    <div class="col-md-6 color-white"><p class="strong">Мы принимаем пожилых людей со следующими заболеваниями:</p> </div>
                </div>
            </div>
        </div>

    </div>
</section>

<section id="comfort">
    <div class="container">
        <div class="row">
            <p class="h1 text-center strong">КОМФОРТ И БЛАГОПОЛУЧИЕ ВАШИХ БЛИЗКИХ</p>

            <div class="col-md-4 text-center"><p class="h3 text-center">Современные интерьеры</p></div>
            <div class="col-md-4 text-center"><p class="h3 text-center">Индивидуальный подход к каждому</p></div>
            <div class="col-md-4 text-center"><p class="h3 text-center">Свободный режим посещений</p></div>
        </div>
    </div>
</section>

<section id="slider">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="slick-slider">
                    <div class="item"><img src="/img/slides/cabinets/1.jpg" alt="" class="img-responsive"></div>
                    <div class="item"><img src="/img/slides/cabinets/2.jpg" alt="" class="img-responsive"></div>
                    <div class="item"><img src="/img/slides/cabinets/3.jpg" alt="" class="img-responsive"></div>
                    <div class="item"><img src="/img/slides/cabinets/4.jpg" alt="" class="img-responsive"></div>
                    <div class="item"><img src="/img/slides/cabinets/5.jpg" alt="" class="img-responsive"></div>
                    <div class="item"><img src="/img/slides/cabinets/6.jpg" alt="" class="img-responsive"></div>
                    <div class="item"><img src="/img/slides/cabinets/7.jpg" alt="" class="img-responsive"></div>
                    <div class="item"><img src="/img/slides/cabinets/8.jpg" alt="" class="img-responsive"></div>
                </div>
            </div>
        </div>
    </div>

</section>

<section id="results">
    <div class="container">
        <div class="row">
            <p class="h2 text-center strong text-uppercase">результаты наших постояльцев</p>

            <div class="col-md-3">
                <p class="heading h5 strong"></p>
                <p class="description"></p>
            </div>
            <div class="col-md-3">
                <p class="heading h5 strong">50% постояльцев</p>
                <p class="description">Положительная динамика состояния здоровья</p>
            </div>
            <div class="col-md-3">
                <p class="heading h5 strong">35% постояльцев</p>
                <p class="description">Стабилизация состояния здоровья</p>
            </div>
            <div class="col-md-3">
                <p class="heading h5 strong">15% постояльцев</p>
                <p class="description">Замедление отрицательной динамики состояния здоровья</p>
            </div>

        </div>
    </div>
</section>

<section class="inline-form form-visit">
    <div class="container">
        <div class="row">
            <p class="h2 text-center text-uppercase strong">записаться на просмотр дома престарелых</p>
            <div class="form clearfix">
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control input" placeholder="Ваше имя">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="tel" class="form-control input" placeholder="Ваш телефон">
                    </div>
                </div>
                <div class="col-md-4">
                    <button class="btn sender btn-warning">Записаться на просмотр дома</button>
                </div>
            </div>
            <p class="text-center">Пожалуйста, вводите свой существующий номер телефона, иначе наш специалист не сможет с Вами связаться.</p>

        </div>
    </div>
</section>

<section id="personal">
    <div class="container">
        <div class="row">
            <p class="h1 text-center strong text-uppercase mt-xlg">ЛЮБИТЕ СВОИХ БЛИЗКИХ</p>
            <p class="h3 text-center mt-none">ОБ ОСТАЛЬНОМ ПОЗАБОТИМСЯ МЫ</p>
            <p class="text-center h1 mb-xlg"><i class="fa fa-heart-o color-red"></i> </p>
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <p>«Мы дружим семьями, постоянно общаемся с родственниками, приглашая к обеду или на чай...»</p>
                <p> Мы дружим с постояльцами семьями, постоянно общаемся с родственниками, приглашая их к обеду или на чай (у нас только домашняя кухня и замечательная выпечка).</p>
                <p>     Нередко к нам привозят людей опустошенных, со стойким депрессивным настроением, тех, кто уже не хочет жить, отказывается от пищи и общения. Но через несколько дней у нас они не просто встают на ноги и начинают ходить, самостоятельно кушают, общаются.</p>
            </div>
        </div>
    </div>
</section>

<section id="garanty">
    <div class="container">
        <div class="row">
            <p class="text-center h1 strong text-uppercase mb-lg mt-xlg">под присмотром 24 часа в сутки 7 дней в неделю</p>
            <div class="col-md-5 col-md-offset-1">
                <ol>
                    <li>Все необходимые гигиенические процедуры</li>
                    <li>Смена личного и постельного белья. Стирка, глажка</li>
                    <li>Контроль за состоянием здоровья и принятием лекарств</li>
                    <li>Пятиразовое сбалансированное питание</li>
                </ol>

            </div>
            <div class="col-md-5">
                <ol start="5">
                    <li>Ежедневная уборка</li>
                    <li>Возможность забирать родственников на любое время</li>
                    <li>Узкие специалисты по необходимости</li>
                    <li>On-line общение с близкими людьми.</li>
            </div>
        </div>
    </div>
</section>

<section id="workers">
    <div class="container">
        <p class="h1 text-center text-uppercase strong">наш персонал</p>
        <div class="row">
            <div class="slick-slider">
                <div class="item">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">АДМИНИСТРАТОР
                        Крючкова Светлана Вячеславовна
                        Опыт работы в аналогичной должности более 20 лет.
                    </div>
                </div>
                <div class="item">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        2.	ПОВАР
                        Новичкова Ольга Романовна
                        Опыт работы 46 лет
                        Повар 5ого разряда
                        1970 г. - Житомирское торгово-кулинарное училище.
                        Специализация: санатории, детские лагеря: База отдыха «Динамо», Дом отдыха «Чайковский», спортивно-оздоровительный центр санаторного типа «Вымпел».
                    </div>
                </div>
                <div class="item">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        3.	СПЕЦИАЛИСТ ПО УХОДУ:
                        Мамаева Гелия Юрьевна
                        Опыт работы 35 лет
                        Медсестра 1ой категории 9ого разряда.
                        1981 г. - Дмитриевское медицинское училище.
                        2011 г. - Тверской медицинский колледж.
                        1981 г – 1982 г. Клинская горбольница – медсестра. Анестезиологическое отделение.
                        1982 г – 2011 г. Зубовская больница – медсестра.
                        2011 г – 2016 г. Отделение социальной реабилитации. Медсестра галокамеры.
                        2016 – Психоневрологический диспансер «Орлово»
                        2016 г. - специалист по уходу за престарелыми в ЧД «Солидный Возраст»

                    </div>
                </div>
                <div class="item">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        4.	СПЕЦИАЛИСТ ПО УХОДУ:
                        Бровенко Ольга Владимировна
                        Опыт работы 21 год
                        Медицинская сестра. Массажист.
                        1995 г. - Стахановское областное медицинское училище.
                        Госпиталь инвалидов ВОВ.
                        Реабилитационный центр «Орбита».
                        Клязминская районная больница.
                        2016 г. - специалист по уходу за престарелыми в ЧД «Солидный Возраст».

                    </div>
                </div>
                <div class="item">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        5.	СПЕЦИАЛИСТ ПО УХОДУ:
                        Новикова Наталья Геннадьевна
                        Опыт работы 20 лет
                        Медицинская сестра.
                        1995 г - Тверской медицинский колледж.
                        1995 г – 2004 г. - Конаковская станция переливания крови.
                        2004 г – 2016 г. – Специалист по уходу за пожилыми людьми и инвалидами на дому.
                        2016 г. - специалист по уходу за престарелыми в ЧД «Солидный Возраст».

                    </div>
                </div>
                <div class="item">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        6.	СПЕЦИАЛИСТ ПО УХОДУ:
                        Вагина-Соколова Ольга Венедиктовна
                        Опыт работы 5 лет
                        Образование среднее специальное. Московская технологическая академия.
                        1999 г – 2005 г. – Воспитатель д/с.
                        2011 г – 2016 г. – Специалист по уходу за пожилыми людьми и инвалидами на дому.
                        2016 г. - специалист по уходу за престарелыми в ЧД «Солидный Возраст».

                    </div>
                </div>
                <div class="item">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">СПЕЦИАЛИСТ ПО УХОДУ
                        Казачухненко Инна Анатольевна
                        Опыт работы 18 лет
                        1998 г. - Медицинское училище № 23 комитета Здравоохранения г. Москвы
                        2016 г. - специалист по уходу за престарелыми в ЧД «Солидный Возраст».
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="dosug">
    <div class="container">
        <div class="row">
            <p class="h2 strong text-center text-uppercase">досуг и развлечения</p>
            <div class="col-md-4"></div>
            <div class="col-md-4">slides</div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <p class="text-center">Мы не скучаем. Активный досуг - лучшая форма сохранения физического и
                    психического здоровья. Прогулки на свежем воздухе, настольные игры, чтение вслух,
                    рисование и лепка, пазлы, танцы, просмотр любимых комедий в уютной гостиной,
                    тематические вечера с пионерами из дома-музея им Гайдара.</p>
                <p class="text-center">Мы делаем выездные дни, дни рождения в кругу родных – все это наша жизнь.</p>
                <p class="text-center">Мы – семья!</p>
            </div>
        </div>
    </div>
</section>

<section id="tariffs">
    <div class="container">
        <div class="row">
            <p class="h2 text-center text-uppercase strong">наши тарифы</p>
            <div class="col-md-4">
                <div class="item">
                    <p class="h3 text-center heading">Программа “Домашний”</p>
                    <p class="description">
                        Предназначен для граждан пожилого возраста, имеющих возможность самостоятельно передвигаться, не нуждающихся в медицинских манипуляциях (инъекции, капельницы и пр.), предпочитающие активный образ жизни.
                    </p>
                    <div class="controls text-center">
                        <button class="more btn btn-default">узнать подробнее</button>
                        <button class="btn btn-warning" data-toggle="modal" data-target="callback_modal">Оставить заявку</button>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="item">
                    <p class="h3 text-center heading">Программа «ПОМОЩНИК»</p>
                    <p class="description">
                        Предназначен для маломобильных граждан пожилого возраста, имеющих возможность самостоятельно
                        передвигаться с помощью технических средств реабилитации (ходунки, коляски, костыли и пр.)
                        внутри помещения и с помощью сопровождающего по территории учреждения, нуждающихся в
                        медицинских манипуляциях (инъекции, капельницы и пр.).
                    </p>
                    <div class="controls text-center">
                        <button class="more btn btn-default">узнать подробнее</button>
                        <button class="btn btn-warning" data-toggle="modal" data-target="callback_modal">Оставить заявку</button>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="item">
                    <p class="h3 text-center heading">Программа «ЗАБОТА»</p>
                    <p class="description">
                       Предназначен для граждан пожилого возраста, нуждающихся
                        в постоянном сопровождении и помощи как медицинской, так и бытовой.
                    </p>
                    <div class="controls text-center">
                        <button class="more btn btn-default">узнать подробнее</button>
                        <button class="btn btn-warning" data-toggle="modal" data-target="callback_modal">Оставить заявку</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="tech">
    <div class="container">
        <div class="row">
            <p class="h1 text-center strong text-uppercase">Современное оборудование</p>
            <div class="col-md-6 col-md-offset-3">
                <ol>
                    <li>Ортопедические матрацы с двумя уровнями жесткости</li>
                    <li>Противопролежневые матрацы</li>
                    <li>Трехсекционные функциональные кровати</li>
                    <li>Ходунки, трости</li>
                    <li>Удобные кресла-каталки</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section id="testimonials">
    <div class="container">
        <div class="row">
            <p class="h1 text-center strong text-uppercase">отзывы</p>
            <p class="h3 text-center bottom-line">Слова от постояльцев</p>
            <div class="col-md-4">
                <div class="item">
                    <p class="heading strong"></p>
                    <p class="text"></p>
                    <p class="strong mt-md"></p>
                    <p class="small date"></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="item">
                    <p class="heading strong"></p>
                    <p class="text"></p>
                    <p class="strong mt-md"></p>
                    <p class="small date"></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="item">
                    <p class="heading strong"></p>
                    <p class="text"></p>
                    <p class="strong mt-md"></p>
                    <p class="small date"></p>
                </div>
            </div>
        </div>

        <div class="text-center">
            <button class="btn btn-default btn-sm">показать еще отзывы</button>
        </div>

    </div>
</section>



<section class="inline-form form-visit">
    <div class="container">
        <div class="row">
            <p class="h2 text-center text-uppercase strong">записаться на просмотр дома престарелых</p>
            <div class="form clearfix">
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control input" placeholder="Ваше имя">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="tel" class="form-control input" placeholder="Ваш телефон">
                    </div>
                </div>
                <div class="col-md-4">
                    <button class="btn sender btn-warning">Записаться на просмотр дома</button>
                </div>
            </div>
            <p class="text-center">Пожалуйста, вводите свой существующий номер телефона, иначе наш специалист не сможет с Вами связаться.</p>

        </div>
    </div>
</section>

<section id="contacts">
    <div class="container">
        <div class="row">
            <p class="h2 strong text-center text-uppercase">контакты</p>
            <div class="col-md-6"></div>
            <div class="col-md-6"></div>
        </div>
        <div class="row" id="map"></div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-md-offset-1">
                <img src="/img/logo.png" class="img-responsive logo">
            </div>
            <div class="col-md-6"></div>
            <div class="col-md-3 text-center">
                <p class="h2 strong color-black" data-value="app_phone">8 (831) 414-21-48</p>
                <p class="h5" data-value="app_address">г. Нижний Новгород, ул. Васенко 4, оф. 306 </p>
            </div>
        </div>
    </div>
</footer>


<div id="upButton"><i class="fa fa-long-arrow-up fa-2x"></i> </div>
<div id="callbackButton"
     class="wow wobble hidden"
     data-wow-delay="3s"
     data-wow-iteration="2"
     data-toggle="modal"
     data-target="#callbackModal"><i class="fa fa-phone"></i> <span class="hidden-xs">Заказать</span> обратный звонок </div>

<div class="modal" id="callbackModal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Заявка на обратный звонок</h4>
            </div>
            <div class="modal-body">
                <p class="h4 text-center mt-none">Закажите обратный звонок и наши менеджеры свяжутся с вами в самое ближайшее время!</p>
                <div class="form-group mb-none">
                    <input data-label="Телефон" type="tel" class="form-control input required mb-none mt-lg text-center" placeholder="Введите номер телефона">
                </div>
            </div>
            <div class="modal-footer text-center">
                <button data-parent="#callbackModal"
                        data-heading="Заявка на обратный звонок"
                        data-action="callback"
                        type="button" class="btn btn-warning center-block sender">Заказать звонок</button>
                <div class="text-center mt-md"><small>Ваши данные в безопасности и никаогда не передаются третьим лицам</small></div>

                <div class="alert responder hidden text-center mt-lg animated zoomIn"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="documentsModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Отправка документов</h4>
            </div>
            <form id="documentsFormForm" action="/engine/ajax.php" method="post"  enctype="multipart/form-data">
                <div class="modal-body" id="documentsForm">
                        <input name="action" value="documents" hidden="hidden">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Ваш телефон</label>
                                    <input type="tel" name="phone" class="form-control input required">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Ваш e-mail</label>
                                    <input name="email" class="form-control input required">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <p class="h2 text-center">Документы</p>
                                <p class="h5 text-center">Загрузите документы архивом .RAR или .ZIP</p>
                                <label class="btn btn-warning btn-file btn-sm btn-block">
                                    Выбрать файл<input id="documentsFormFile" multiple="multiple" type="file" name="file[]" class="required">
                                </label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="documentsFormFiles"></div>
                                    </div>
                                </div>
                                <hr>
                                <p class="h5 text-center">Вам понадобится:</p>
                                <ul>
                                    <li>Свидетельство о регистрации транспортного средства (СТС)/Паспорт транспортного средства (ПТС) - обе стороны</li>
                                    <li>Паспортные данные страхователя (страница с фотографией и страница с регистрацией)</li>
                                    <li>Паспортные данные собственника (страница с фотографией и страница с регистрацией)</li>
                                    <li>Водительские удостоверения всех лиц, допущенных к управлению транспортным средством- обе стороны</li>
                                    <li>Диагностическая карта - обе стороны</li>
                                </ul>
                            </div>
                        </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="submit" class="btn btn-warning center-block"><i class="fa fa-paper-plane-o"></i> Отправить</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.maskedinput.min.js"></script>
<script src="/js/wow.js"></script>
<script src="/js/slick.min.js"></script>
<script src="/js/forms.js"></script>
<script src="/js/scripts.js"></script>


</body>
</html>