/**
 * Created by Alex on 27.11.2016.
 */

var cities = [
    ["", 0],
    ["Нижний Новгород", 1.8],
    ["Арзамас", 1.1],
    ["Выкса", 1.1],
    ["Саров", 1.1],
    ["Балахна", 1.3],
    ["Бор и Борская область", 1.3],
    ["Дзержинск", 1.3],
    ["Кстово", 1.2],
    ["Прочие города (Нижегородская облать)", 1]
];

var cars = [
    ["ACURA", "AUDI"],
    ["BENTLEY", "BMW"],
    ["CADILLAC","CHERY","CHEVROLET","CHRYSLER","CITROEN"],
    ["DAEWOO","DODGE"],
    ["FIAT", "FORD"],
    ["GEELY", "GREAT WALL"],
    ["HONDA","HUMMER","HYUNDAI"],
    ["INFINITI", "ISUZU"],
    ["JAGUAR", "JEEP"],
    ["KIA"],
    ["LAND ROVER","LEXUS","LIFAN","LINCOLN"],
    ["MAZDA","MERCEDES BENZ","MINI","MITSUBISHI"],
    ["NISSAN"],
    ["OPEL"],
    ["PEUGEOT","PORSCHE"],
    ["RENAULT"],
    ["SAAB", "SEAT", "SKODA", "SMART", "SSANGYONG", "SUBARU", "SUZUKI"],
    ["TOYOTA"],
    ["VOLKSWAGEN", "VOLVO"],
    ["ВАЗ"],
    ["ГАЗ"],
    ["ЗАЗ",
    "ТАГАЗ"],
    ["УАЗ"]];

var cars_cargo = [
	["Acerbi",	"Adria",	"ANKAI",	"Asia",	"ASTRA",	"Avia"],
	["Barkas",	"BAW",	"BMC",	"Bova"],
	["Caravan",	"ChangFeng",	"Chevrolet",	"Citroen"],
	["Daewoo",	"DAF",	"Daihatsu",	"Dodge",	"DongFeng"],
	["Faun",
	"Faw",
	"Fiat",
	"Fliegl",
	"Ford",
	"Foton",
	"Freightliner"],
	["Geely",
	"GMC",
	"Golden Dragon",
	"Great Wall"],
["Hapert",
	"Higer",
	"Hino",
	"Howo",
	"Hymer",
	"Hyundai"],
["IFA",
	"Ikarus",
	"International",
	"Intrall",
	"Irisbus",
	"Iritobus",
	"Isuzu",
	"Iveco"],
["Jac",
	"JCB",
	"JMC"],
["Kato",
	"Kenworth",
	"Kia",
	"Kip",
	"Koegel",
	"Krupp"],
["LDV",
	"Lincoln",
	"Lublin"],
["Mack",
	"MAN",
	"Mazda",
	"Mercedes-Benz",
	"Mitsubishi",
	"Mudan"],
["Neoplan",
	"Nissan"],
["Opel"],
["Pacton",
	"Peterbilt",
	"Peugeot"],
["Renault"],
["Scania",
	"Scherau",
	"Schmitz",
	"Setra",
	"Shenye",
	"SISU",
	"Skoda",
	"Sommer",
	"SsangYong",
	"Sterling",
	"Suzuki"],
["Tadano",
	"TATA Daewoo",
	"Tatra",
	"Temsa",
	"Terberg",
	"Toyota"]
];


var Class = function() {
    var klass = function(){
        this.init.apply(this, arguments);
    };
    klass.prototype.init = function() {};
    return klass;
};

// constructor
var Calculator = new Class;
Calculator.prototype.basePrice = 0;
Calculator.prototype.step = 0;
Calculator.prototype.properties = {
    city: 0,
    carName: "",
    carPower: 0,
    carPowerText: "",
    carAge: 2000,
    driverAge: 18,
    driverExperience: 0,
    noLimitInsurance: 0
};

Calculator.prototype.control_button = {};

Calculator.prototype.init = function(items){
    var me = this;
    me.properties = {
        city: 0,
        carName: "",
        carPower: 0,
        carPowerText: "",
        carAge: 2000,
        driverAge: 18,
        driverExperience: 0,
        noLimitInsurance: 0,
        isCargo: 0,
        discount: 1
    };
    this.cities = cities;
    this.cars = cars;
    this.cargo_cars = cars_cargo;
    this.basePrice = 4118;
    this.basePrice_cargo_below_16 = 4211;
    this.basePrice_cargo_more_16 = 5284;
    this.total = 0;
    this.step = 1;
    this.control_button =  $('#calculator_control_button');

    $('.calculator-step').addClass('hidden');
    $(".calculator-step[data-value='1']").removeClass('hidden');
    
   /* var parent = document.getElementById('city_selector');
    $(parent).html("");
    for (var i = 0; i < this.cities.length; i++) {
        var createdItem = document.createElement('option');
        createdItem.text = this.cities[i][0];
        createdItem.setAttribute("data-value", this.cities[i][1]);
        createdItem.setAttribute("data-id", [i]);
        i == 0 ? createdItem.setAttribute("data-selected", "selected") : "";

        parent.appendChild(createdItem);
    }

    $('#city_selector').on('change', function () { me.goToStep(2); });*/


    // города
    var parent_city = document.getElementById('city_selector');
    $(parent_city).html("");

    for (var j = 1; j < this.cities.length; j++) {
        var group = document.createElement('div');
        group.className = "col-md-3 col-xs-6";

        var elem = document.createElement('div');
            elem.className = 'checkbox-inline';
            elem.innerHTML = this.cities[j][0];
            if (elem.innerHTML.length > 20) {group.className = "col-md-4"}
            if (elem.innerHTML.length > 26) {group.className = "col-md-6"}
            $(elem).attr('data-value',j);
            group.appendChild(elem);

        parent_city.appendChild(group);
    }

    $('#city_selector .checkbox-inline').click(function () {
        // clear all other
        $('#city_selector .checkbox-inline').removeClass('checked');
        $(this).addClass('checked');
        calculator.properties.city = parseInt($(this).attr('data-value'));
        me.goToStep(2);
    });


    // легковые
    var parent_car = document.getElementById('calculator_carName');
    $(parent_car).html("");
    var elem = document.createElement('div');
    elem.className = 'checkbox-inline bordered';
    elem.innerHTML = "Другая марка";
    parent_car.appendChild(elem);

    for (var j = 0; j < this.cars.length; j++) {
        var group = document.createElement('div');
            group.className = "col-md-4";
            group.innerHTML = "<p class='h4'>" + this.cars[j][0][0] + "</p>";
            for (var l=0; l < this.cars[j].length; l++){
                var elem = document.createElement('div');
                    elem.className = 'checkbox-inline';
                    elem.innerHTML = this.cars[j][l];
                  group.appendChild(elem);
            }
        parent_car.appendChild(group);
    }

    $('#calculator_carName .checkbox-inline').click(function () {
        // clear all other
        $('#calculator_carName .checkbox-inline').removeClass('checked');
        $(this).addClass('checked');
        calculator.properties.carName = $(this).html();
        calculator.properties.isCargo = 0;
        me.goToStep(3);
        $('#calculator').scrollView();
    });

    // грузовые

   /* var parent_car = document.getElementById('calculator_carName_cargo');
    $(parent_car).html("");

    for (var j = 0; j < this.cargo_cars.length; j++) {
        var group = document.createElement('div');
        group.className = "col-md-4";
        group.innerHTML = "<p class='h4'>" + this.cargo_cars[j][0][0] + "</p>";
        for (var l=0; l < this.cargo_cars[j].length; l++){
            var elem = document.createElement('div');
            elem.className = 'checkbox-inline';
            elem.innerHTML = this.cargo_cars[j][l];
            group.appendChild(elem);
        }
        parent_car.appendChild(group);
    }

    $('#calculator_carName_cargo .checkbox-inline').click(function () {
        // clear all other
        $('#calculator_carName_cargo .checkbox-inline').removeClass('checked');
        $(this).addClass('checked');
        calculator.properties.carName = $(this).html();
        calculator.properties.isCargo = 1;
        me.goToStep(3);
    });*/

    $('#calculator_carName_cargo .checkbox-inline').click(function () {
        // clear all other
        $('#calculator_carName_cargo .checkbox-inline').removeClass('checked');
        $(this).addClass('checked');
        calculator.properties.carName = $(this).html();
        calculator.properties.isCargo = parseInt($(this).attr('data-value'));
        me.goToStep(3);
    });

    $('#calculator_carAge .checkbox-inline').click(function () {
        // clear all other
        $('#calculator_carAge .checkbox-inline').removeClass('checked');
        $(this).addClass('checked');
        if ($(this).attr('data-value') == 'older'){
            $('#calculator_carAge .form-group').removeClass('hidden');
        } else {
            $('#calculator_carAge .form-group').addClass('hidden');
            me.properties.carAge = $(this).attr('data-value');
            me.goToStep(4);
        }
    });

    $('#calculator_carAge_manual').click(function() {
        var age = parseInt($("input[data-value='calculator_carAge']").val());
        if ( age > 0) {
            me.properties.carAge = age;
            me.goToStep(4);
        }
    });

    $('#calculator_carPower .checkbox-inline').click(function () {
        // clear all other
        $('#calculator_carPower .checkbox-inline').removeClass('checked');
        $(this).addClass('checked');
        calculator.properties.carPower = $(this).attr('data-value');
        me.goToStep(5);
    });

    $('#calculator_noLimitInsurance').click(function () {
        // clear all other
        $(this).toggleClass('checked');
        if ($(this).hasClass('checked')){
            calculator.properties.noLimitInsurance = 1;
            $('#calculator_driverAge').addClass('semitransparent');
            me.goToStep(6);
        } else {
            calculator.properties.noLimitInsurance = 0;
            $('#calculator_driverAge').removeClass('semitransparent');
        }
    });

    $('#calculator_driverExperience_manual').click(function(){
        calculator.properties.noLimitInsurance = 0;
        me.goToStep(6);
    });

    $('#calculator_breadcrumbs .item').click(function () {
        var step = $(this).attr('data-step');
        $(this).addClass('hidden');

        var items = $("#calculator_breadcrumbs .item");
        for (var i=0; i < (items.length) ; i++){
            var item = items[i];
            if (parseInt($(item).attr('data-step'))  > step) { $(item).addClass('hidden'); }
        };

        me.goToStep(parseInt(step));
    });
};

Calculator.prototype.refreshView = function () {

};

Calculator.prototype.calculate = function() {
    // установка значений для расчета
    // базовая стоимость (для легковых)
    var price = this.basePrice;

    // базовая стоимость для грузовых авто до 16 тонн
    if (this.properties.isCargo == 10){
        price = this.basePrice_cargo_below_16;
    }
    // базовая стоимость для грузовых авто более 16 тонн
    if (this.properties.isCargo == 20){
        price = this.basePrice_cargo_more_16
    }

    // коэффициент города. хранится в массиве cities
    var city_coefficiend = cities[this.properties.city][1];
    // коэффициент мощности двигателя по умолчанию
    var power_coefficient = 1;
    // коэффициент мощности двигателя
    switch (this.properties.carPower){
        case 49:
            power_coefficient = 0.6;
            break;
        case 51:
            power_coefficient = 1;
            break;
        case 71:
            power_coefficient = 1.1;
            break;
        case 101:
            power_coefficient = 1.2;
            break;
        case 121:
            power_coefficient = 1.4;
            break;
        case 151:
            power_coefficient = 1.6;
            break;
        default:
            power_coefficient = 1;
            break;
    }

    // коэффициент водителя - по умолчанию
    var driver_coefficient = 1;
    // вычисление коэффициента для водителя
    // страховка без ограничений
    if (this.properties.noLimitInsurance == 1){
        driver_coefficient = 1.8;
    } else {
        // условия для обычных страховок в зависимости от стажа и возраста водителя
        if (this.properties.driverAge <= 22 && this.properties.driverExperience < 3){
            driver_coefficient = 1.8;
        }

        if (this.properties.driverAge <= 22 && this.properties.driverExperience > 3){
            driver_coefficient = 1.6;
        }

        if (this.properties.driverAge > 22 && this.properties.driverExperience < 3){
            driver_coefficient = 1.7;
        }

        if (this.properties.driverAge > 22 && this.properties.driverExperience > 3){
            driver_coefficient = 1;
        }
    }

    // расчет скидки
    var discount = 1;
    if (this.properties.driverExperience == 1){ discount = 0.95; }
    if (this.properties.driverExperience == 2){ discount = 0.90; }
    if (this.properties.driverExperience == 3){ discount = 0.85; }
    if (this.properties.driverExperience == 4){ discount = 0.80; }
    if (this.properties.driverExperience == 5){ discount = 0.75; }
    if (this.properties.driverExperience >= 6){ discount = 0.7; }

    // расчет итогового ОСАГО - базовая стоимость * коэфф. города * коэфф. мощности * коэфф. страховки + 1500 - скидка
    this.properties.discount = discount;
    var const_upprice = 1500;
    this.total = parseInt((price  * discount)  * city_coefficiend * power_coefficient * driver_coefficient + const_upprice);
    this.properties.total = this.total;
    return this.total;
};

Calculator.prototype.goToStep = function (step) {
    var me = this;
    var validated = true;
    switch (step){
        case 1:
            validated = true;
            break;
        case 2:
                $('#calculator .steps .step .filled').removeClass('filled');
                $($('#calculator .steps .step')[1]).find('.count').addClass('filled');
            $('#calculator_breadcrumbs [data-value="city"] span').html(cities[me.properties.city][0]).parent().removeClass('hidden');
            validated = true;
            break;
        case 3:
            if (me.properties.carName != ""){
                $('#calculator_breadcrumbs [data-value="carName"] span').html(me.properties.carName).parent().removeClass('hidden');

            } else {
                validated = false;
            }
            break;
        case 4:
            if (me.properties.carAge > 0){
                    $('#calculator_breadcrumbs [data-value="carAge"] span').html( me.properties.carAge ).parent().removeClass('hidden');
                }else{ validated = false; }
            break;
        case 5:
            if ($('#calculator_carPower .checked').length > 0){
                me.properties.carPower = parseInt($('#calculator_carPower .checked').attr('data-value'));
                me.properties.carPowerText = ($('#calculator_carPower .checked').attr('data-text'));
                $('#calculator_breadcrumbs [data-value="carPower"] span').html( me.properties.carPowerText ).parent().removeClass('hidden');
            }else{ validated = false; }
            break;
        case 6:
            // if no limit
            if ($('#calculator_noLimitInsurance').hasClass('checked')) {
                me.properties.noLimitInsurance = 1;
                $('#calculator_breadcrumbs [data-value="insurance"] span').html( 'Без ограничений' ).parent().removeClass('hidden');
            } else {   me.properties.noLimitInsurance = 0;}
            // if common
            if (me.properties.noLimitInsurance == 0)
            {
                if ($('#calculator_driverAge [data-value="driverAge"]').val() != "" && $('#calculator_driverAge [data-value="driverExperience"]').val() != ""){
                    me.properties.driverAge = parseInt($('#calculator_driverAge [data-value="driverAge"]').val());
                    me.properties.driverExperience = parseInt($('#calculator_driverAge [data-value="driverExperience"]').val());
                    $('#calculator_breadcrumbs [data-value="insurance"] span').html(    me.properties.driverAge + " лет / из них стажа: " + me.properties.driverExperience).parent().removeClass('hidden');
                } else {
                    if ($('#calculator_driverAge [data-value="driverAge"]').val() == "") {
                        $('#calculator_driverAge [data-value="driverAge"]').parent().addClass('has-error');
                    }
                    if ($('#calculator_driverAge [data-value="driverExperience"]').val() == "") {
                        $('#calculator_driverAge [data-value="driverExperience"]').parent().addClass('has-error');
                    }
                    validated = false;
                }
            }

            if (validated){
                $('#calculator .steps .step .filled').removeClass('filled');
                $($('#calculator .steps .step')[2]).find('.count').addClass('filled');
                $(me.control_button).addClass('hidden');
                $('#calculator_result').html(parseInt(me.calculate()));
                $('.calculator-step[data-value="6"] .sender').removeClass('hidden');
                $('#calculator_form .alert').addClass('hidden');
            }
            break;
        case 7:
            // form

            validated = false;
            break;
        default:
            validated = false;
            break;
    }

    if (validated){
        $(this.control_button).attr('data-step', step);
        $(".calculator-step").addClass('hidden');
        $(".calculator-step[data-value='"+step+"']").removeClass('hidden');
        me.refreshView();
    } else { console.log('err');}
};

Calculator.prototype.goToBackStep = function (step) {

};


var calculator;

$(function () {
    calculator = new Calculator(cities);
});

