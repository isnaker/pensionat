/**
 * Created by Alex on 28.03.2016.
 */


var files;
$(function(){
    $('.sender').click(function(){
        var validated = true;
        var thus = this;
        var parent = $( $(this).attr('data-parent') );
        var fields = $(parent).find('.input');
        var action = "callback";
        var message = "<h3>" + $(this).attr('data-heading') + "</h3>";

        if ($(this).attr('data-action') != ""){ action = $(this).attr('data-action'); }


        for (var i=0; i < fields.length; i++){
            if ($(fields[i]).hasClass('required') && $(fields[i]).val() == ""){
                $(fields[i]).parent().addClass('has-error');
                validated = false;
            }
        }

        if (validated){
            for (var i=0; i < fields.length; i++){
                message += $(fields[i]).attr('data-label') + ": <strong>"+$(fields[i]).val()+"</strong><br>";
            }

            if (action == "calc"){
                var cartext = calculator.properties.carName;
                if (calculator.properties.isCargo > 0){
                    if (calculator.properties.isCargo == 10){
                        cartext = "Грузовая до 16 т.";
                    }
                    if (calculator.properties.isCargo == 20){
                        cartext = "Грузовая более 16 т.";
                    }
                }

                var insuranceText = "";
                if (calculator.properties.noLimitInsurance == 1){
                    insuranceText = "Страховка без ограничений";
                } else {
                    insuranceText = "Для водителя " + calculator.properties.driverAge + " лет с опытом " + calculator.properties.driverExperience + " лет";
                }

                message += "<hr><h3>---- Введенные данные для расчета страховки ----</h3>";
                message += "Город: <strong>" + cities[calculator.properties.city][0] + "</strong><br>";
                message += "Марка авто: <strong>" + cartext + "</strong><br>";
                message += "Мощность авто: <strong>" + calculator.properties.carPowerText + "</strong><br>";
                message += "Год выпуска авто: <strong>" + calculator.properties.carAge + "</strong><br>";
                message += "Страховка: <strong>" + insuranceText + "</strong><br>";
                message += "<br>Стоимость страховки: <strong>" + calculator.properties.total + " руб.</strong><br>";
                message += "Скидка: <strong>" + (100 - (calculator.properties.discount * 100)) + "%</strong><br>";
            }


            var data = {
                action: action,
                message: message
            };

            $.ajax({
                type: 'POST',
                url: '/engine/ajax.php',
                data: data,
                success: function () {
                    $(parent).find('.responder').html("Сообщение успешно отправлено!")
                        .addClass('alert-success')
                        .removeClass('alert-danger')
                        .removeClass('hidden');

                    $(thus).addClass('hidden');
                    yaCounter40697859.reachGoal('zayavka');
                },
                timeout: function () {
                    _response = 0;
                },
                error: function () {
                    _response = 0;
                }
            });


        } else {
            //$(fields).parent().addClass('has-error');

            $(parent).find('.responder').html("Ошибка! Вы не заполнили все необходимые поля!")
                .addClass('alert-danger')
                .removeClass('alert-success')
                .removeClass('hidden');
        }
    });


    $('#documentsFormFile').on('change', function () {
        var files = this.files;
        var container = document.getElementById('documentsFormFiles');
            $(container).html("<p class='h4 text-center'>Выбранные файлы:</p>");
        for (var i = 0; i < files.length; i++){
                var item = document.createElement('div');
                    item.className = "item";
                    item.innerHTML = "<i class='fa fa-check'></i> " + files[i].name;
            container.appendChild(item);
        }
    });

    $('#documentsFormForm').on('submit', function() {

        if ($('#documentsFormForm').find('.required').val() == ""){
            $('#documentsFormForm').find('.required').parent().addClass('has-error');
            return false;
        }

    });
    
    
    
});