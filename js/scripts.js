$.fn.scrollView = function () {
    return this.each(function () {
        $('html, body').animate({
            scrollTop: $(this).offset().top
        }, 500);
    });
};

$(function(){
    $('input[type="tel"]').mask('+7 (999) 999-99-99');
    $('input, select').focus(function(){
        $(this).parent().removeClass('has-error');
    });

    $(window).scroll(function(){
        var scroll = $(window).scrollTop();

        if (scroll > 150){
          //  $('#stick_navigation').addClass('fixed');
            $('#upButton').addClass('active');
        } else {
          // $('#stick_navigation').removeClass('fixed');
            $('#upButton').removeClass('active');
        }
    });

    // setup app geo's change
   /* var app_addresses_selector = document.getElementById('app_city_selector');
    for (var i=0; i < $.addresses.length; i++){
        var item = document.createElement('option');
            item.innerHTML = $.addresses[i].name;
            $(item).attr('data-value',i);
        app_addresses_selector.appendChild(item);
    }

    $('#app_city_selector').on('change', function () {
        var id = this.selectedIndex;
        $('[data-value="app_phone"]').html($.addresses[id].phone);
        $('[data-value="app_address"]').html($.addresses[id].address);
        $('.map').addClass('hidden');
        $($.addresses[id].mapId).removeClass('hidden');
    });
*/


    // setup scroll up button
    $('#upButton').click( function () {
        $(window.opera ? 'html' : 'html, body').animate({
            scrollTop: 0
        }, 1000); // scroll takes 1 second
    });

   // $('.fancybox').fancybox();
    new WOW().init();
    $('.slick-slider').slick({
        arrows: true,
        dots: true,
    });
    
    $('.slick-carousel').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
});


