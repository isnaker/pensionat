<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 17.11.2015
 * Time: 20:50
 */

require ($_SERVER['DOCUMENT_ROOT'].'/engine/phpmailer/PHPMailerAutoload.php');

$input = $_POST;
$response = array();

if (isset($input['action'])){
    $response['action'] = $input['action'];
    if (isset($input['message'])){ $response['message'] = $input['message']; }

    switch ($input['action']){
        case "callback":
            $response['response'] = sendMail($input);
            break;
        case "calc":
            $input['heading'] = "Заявка с калькулятора";
            $response['response'] = sendMail($input);
            break;
        case "documents":
            $input['heading'] = "Документы для расчета страховки";
            $response['response'] = sendMail($input);
            break;
        default:
            break;
    }
    print json_encode($response);
}

// set up mailer

function sendMail($data){
    $mail = new PHPMailer;
    $mail->CharSet="utf-8";
    $mail->setFrom('robot@osago.ru','Осаго');
    $mail->addAddress('mail@snapix.ru', 'Snapix');
    $mail->addAddress('nn@a-osago.ru', 'Kukuev');

    if (isset($data['heading'])){$mail->Subject = $data['heading'];} else {
        $mail->Subject = 'Новая заявка!';
    }

    if (isset($data['message'])){ $mail->msgHTML($data['message']); }



    if ($data['action'] == "documents"){
        if (isset($_FILES['file'])) {
            for ($i = 0; $i < count($_FILES['file']['name']); $i++ ) {
                if ($_FILES['file']['error'][$i] == UPLOAD_ERR_OK) {
                    $mail->AddAttachment($_FILES['file']['tmp_name'][$i], $_FILES['file']['name'][$i]);
                }
            }
            $mail->msgHTML("<h2>Документы для расчета страховки</h2><br>Телефон клиента: <strong>".$data['phone']."</strong><br>E-mail:<strong>".$data['email']."</strong><p>Документы во вложении.</p><hr><p>Создано ".date('d.m.Y, в hh.mm')."</p>");
        }

        session_start();
        if (!$mail->send()){
            $_SESSION['message_result']['class'] = "warning";
            $_SESSION['message_result']['text'] = "Произошла ошибка при отправке сообщения";
        } else {
            $_SESSION['message_result']['class'] = "success";
            $_SESSION['message_result']['text'] = "Ваше сообщение отправлено! Мы свяжемся с вами в самое ближайшее время!";
        }
        
        header('Location: /');
        
    } else {
        if(!$mail->send()) {
            return 'Message could not be sent.'.$mail->ErrorInfo;
        } else {
            return 'Message has been sent';
        }
    }
}


